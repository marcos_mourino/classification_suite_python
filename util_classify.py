import sqlalchemy
import nltk
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,scoped_session
from db_simplified_declarative import Base, Document, Annotation
from scipy import spatial
import json
import random
import numpy as np
from numpy.linalg import inv
from numpy.linalg import pinv
import time
from numpy.linalg import lapack_lite

from nltk.stem.porter import *
from nltk.tokenize import RegexpTokenizer


#EngineDB = create_engine('mysql://classify_user:classify_password@localhost/simplified_reuters_27000_annotated_with_title_and_description')
#Base.metadata.bind = EngineDB
#DBSession = sessionmaker(bind=EngineDB)
#Session = DBSession()

EngineDB = None
DBSession = sessionmaker()
Session = DBSession()

def set_database_session(corpus):
    global Sesionmaker, EngineDB, DBSession, Session
    
    if corpus == "boc_reuters_27000" or corpus == "bow_reuters_27000":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_reuters_27000_annotated_with_title_and_description_01'

    elif corpus == "boc_reuters_21578" or corpus == "bow_reuters_21578":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_reuters_21578_threshold_01'

    elif corpus == "boc_reuters_21578_expanded":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_reuters_21578_th_01_expanded'    

    elif corpus == "boc_ohsumed" or corpus == "bow_ohsumed":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_ohsumed_threshold_01'

    elif corpus == "boc_ohsumed_expanded":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_ohsumed_th_01_expanded'
        
    elif corpus == "boc_20_newsgroup" or corpus == "bow_20_newsgroup":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_20newsgroups_threshold_01'

    elif corpus == "boc_20_newsgroup_expanded":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_20newsgroups_th_01_expanded'        
    
    elif corpus == "boc_ieee" or corpus == "bow_ieee":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_ieee_threshold_01'

    elif corpus == "boc_ieee_expanded":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_ieee_th_01_expanded'

    elif corpus == "bow_oercommons":
        db_parameters = 'mysql://classify_user:classify_password@192.168.1.12/simplified_oercommons_threshold_01'
    
    EngineDB = create_engine(db_parameters)
    #Session.remove()
    Base.metadata.bind = EngineDB
    DBSession.configure(bind=EngineDB)    
    Session = DBSession()

def cosine_measure(v1,v2):
    return spatial.distance.cosine(v1,v2)

def jaccard_measure(v1,v2):
    return spatial.distance.jaccard(v1,v2)

def braycurtis_measure(v1,v2):
    return spatial.distance.braycurtis(v1,v2)

def squared_euclidean_measure(v1,v2):
	return spatial.distance.sqeuclidean(v1,v2)

def canberra_measure(v1,v2):
	return spatial.distance.canberra(v1,v2)

def manhattan_measure(v1,v2):
	return spatial.distance.cityblock(v1,v2)

def chebyshev_measure(v1,v2):
	return spatial.distance.chebyshev(v1,v2)

def mahalanobis_measure(v1,v2):
    #print v1
    #print v1.shape
    #print v2
    if (v1==v2).all() == True:
        return 0
    else:
    	z = zip(v1,v2)
        cov_matrix = np.cov(z)
        #print "cov matrix"
    	#print cov_matrix
    	#print "cov matrix xhape"
    	#print cov_matrix.shape
        inverse_cov_matrix = pinv(cov_matrix) #pinv evita que de un pete cuando las matrices son singulares 
        #print "inverse cov matrix shape"
    	#print inverse_cov_matrix.shape
        return spatial.distance.mahalanobis(v1,v2,inverse_cov_matrix)

def get_categories(corpus): 
    return [category[0] for category in Session.query(Document.original_category).distinct()]              

def get_multiple_categories(corpus):
    categories = []
    for category in Session.query(Document.original_category).distinct():
        tokens = [x.strip() for x in category[0].split(',')]
        for i in tokens:
            categories.append(i)
    categories = list(set(categories))
    return categories

def get_document_annotations(annotations,weighting, threshold, expansion_threshold, expansion_relatedness, expanded_weighting):
    if expansion_threshold < 1:
        expansion = True
    else:
        expansion = False
        
    aux_document_annotations = []
    for annotation in annotations:
        if annotation.weight >= threshold:
            if weighting == "milne":
                aux_weighting = annotation.weight
            elif weighting == "binary":
                aux_weighting = 1
            aux_document_annotations.append((annotation.name,aux_weighting))            
            if expansion == True and annotation.weight >= expansion_threshold:                
                for annotation_expanded in annotations:                   
                    if annotation_expanded.expanded == True and annotation_expanded.expanded_from == annotation.old_id and annotation_expanded.relatedness >= expansion_relatedness:
                        aux_document_annotations.append((annotation_expanded.name,expanded_weighting))
    return aux_document_annotations
    

def get_documents_from_database_boc(corpus,threshold, weighting, expansion_threshold, expansion_relatedness, num_documents_per_category_for_training,expanded_weighting,num_documents_per_category_for_testing = 0):
    print "--- METODO: get_documents_from_database_boc() ---"
    print expanded_weighting
    documents_train = []
    documents_test = []
        
    if "reuters_27000" in corpus or "ieee" in corpus:
        categories = get_categories(corpus)
        ubicacion = categories.index("General Topics for Engineers .LB.Math  Science .AND. Engineering.RB.")
        del categories[ubicacion]
        for category in categories:
            total_documents_by_category = Session.query(Document).filter(Document.original_category == category).count()        
            for document in Session.query(Document).filter(Document.original_category == category)[:num_documents_per_category_for_training]:
                documents_train.append((document.id, document.original_category, get_document_annotations(document.annotations, weighting, threshold, expansion_threshold, expansion_relatedness,expanded_weighting)))                 
                #    documents_train.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))                                                                                                                                                                    
                                       
            if num_documents_per_category_for_testing > 0:          
                for document in Session.query(Document).filter(Document.original_category == category)[total_documents_by_category - num_documents_per_category_for_testing: total_documents_by_category]:#num_documents_per_category_for_training:num_documents_per_category_for_training+num_documents_per_category_for_testing]:
                    documents_test.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))
                    
        if num_documents_per_category_for_testing == 0:
            return documents_train
        else:   
            return documents_train, documents_test
                                      
    else:
        categories = get_categories(corpus)     
        #### TRAINING ####
        if num_documents_per_category_for_training == 0:
            print "Obteniendo documents de training de todo el corpus " + corpus
            for document in Session.query(Document).filter(Document.cgisplit == "train"):
                documents_train.append((document.id, document.original_category, get_document_annotations(document.annotations, weighting, threshold, expansion_threshold, expansion_relatedness)))                 
                #documents_train.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))  
        else:
            print "Obteniendo documents de training de una parte del corpus " + corpus
            num_train = 0 
            for category in categories:
                for document in Session.query(Document).filter(Document.original_category == category).filter(Document.cgisplit == "train"):
                    documents_train.append((document.id, document.original_category, get_document_annotations(document.annotations, weighting, threshold, expansion_threshold, expansion_relatedness, expanded_weighting)))                                
                    if num_train >= num_documents_per_category_for_training:
                        break;
                    else:
                        num_train+=1
                num_train = 0

        #### TEST ####                         
        if num_documents_per_category_for_testing == 0:                
            print "Obteniendo documents de test de todo el corpus " + corpus                                                                                                 
            for document in Session.query(Document).filter(Document.cgisplit == "test"):
                if weighting == "milne":
                    documents_test.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))
                elif weighting == "binary":
                    documents_test.append((document.id, document.original_category, [(annotation.name,1) for annotation in document.annotations if annotation.weight >= threshold]))
        else:
            print "Obteniendo documents de test de una parte del corpus " + corpus
            num_test = 0
            for category in categories:
                for document in Session.query(Document).filter(Document.original_category == category).filter(Document.cgisplit == "test"):
                    if weighting == "milne":                                                         
                        documents_test.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))
                    elif weighting == "binary":
                        documents_test.append((document.id, document.original_category, [(annotation.name,1) for annotation in document.annotations if annotation.weight >= threshold]))
                    if num_test >= num_documents_per_category_for_testing:
                        break;
                    else:
                        num_test+=1
                num_test = 0     
                       
        return documents_train, documents_test
                
        


def get_documents_from_database_bow(corpus, num_documents_per_category_for_training,num_documents_per_category_for_test = 0):
    print "--- METODO: get_documents_from_database_bow() ---"
    
    stemmer = PorterStemmer()
    tokenizer =  RegexpTokenizer(r'\w+')
    
    documents_training = []
    documents_test = []       
         
    if "reuters_27000" in corpus or "ieee" in corpus:
        categories = get_categories(corpus)
        print "Obteniendo documentos de training y de test de una parte del corpus " + corpus    
        for category in categories:
            #print "- Category: %s " % category        
            total_documents_by_category = Session.query(Document).filter(Document.original_category == category).count()
            #print total_documents_by_category
            for document in Session.query(Document).filter(Document.original_category == category)[:num_documents_per_category_for_training]:                        
                documents_training.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(str(document.name) + "." + document.description)]))            
                                                   
            if num_documents_per_category_for_test > 0:          
                for document in Session.query(Document).filter(Document.original_category == category)[total_documents_by_category - num_documents_per_category_for_test: total_documents_by_category]:#num_documents_per_category_for_training:num_documents_per_category_for_training+num_documents_per_category_for_test]:
                    documents_test.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(str(document.name) + "." + document.description)]))                                
    
        if num_documents_per_category_for_test == 0:
            return documents_training
        else:   
            return documents_training, documents_test
        
    #RESTO DE CORPUS
    else:
        categories = get_categories(corpus)        
        ####### TRAINING #######             
        if num_documents_per_category_for_training == 0:
            print "Obteniendo documents de training de todo el corpus" + corpus            
            for document in Session.query(Document).filter(Document.cgisplit == "train"):                    
                documents_training.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description)] ))
        else:
            num_train = 0
            print "Obteniendo documents de training de una parte del corpus " + corpus                                                                                                
            for category in categories:
                for document in Session.query(Document).filter(Document.original_category == category).filter(Document.cgisplit == "train"):                    
                    documents_training.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description)]))                    
                    if num_train >= num_documents_per_category_for_training:
                        break;
                    else:
                        num_train+=1
                num_train = 0            
            
        ######## TEST ##################
        if num_documents_per_category_for_test == 0:
            print "Obteniendo documents de test de todo el corpus " + corpus                                                                                                 
            for document in Session.query(Document).filter(Document.cgisplit == "test"):
                documents_test.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description)]))
        else:
            num_test = 0
            print "Obteniendo documents de test de una parte del corpus " + corpus                                                                                                
            for category in categories:     
                for document in Session.query(Document).filter(Document.original_category == category).filter(Document.cgisplit == "test"):
                    documents_test.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description)]))                    
                    if num_test >= num_documents_per_category_for_test:
                        break;
                    else:
                        num_test+=1               
                num_test = 0 
            
        return documents_training, documents_test


  

def get_documents_from_multilabel_database_boc(corpus,threshold, weighting, expansion_threshold, expansion_relatedness, num_documents_for_training,expanded_weighting,num_documents_for_test = 0):
    print "--- METODO: get_documents_from_multilabel_database_boc() ---"
    documents_train = []
    documents_test = []   
    #### TRAINING ####
    if num_documents_for_training == 0:
        print "Obteniendo documents de training de todo el corpus " + corpus
        for document in Session.query(Document).filter(Document.cgisplit == "train"):
            documents_train.append((document.id, document.original_category, get_document_annotations(document.annotations, weighting, threshold, expansion_threshold, expansion_relatedness, expanded_weighting)))                 
        random.seed("www.itec-sde.net")
        random.shuffle(documents_training)
    else:
        print "Obteniendo documents de training de una parte del corpus " + corpus
        for document in Session.query(Document).filter(Document.cgisplit == "train"):
            documents_train.append((document.id, document.original_category, get_document_annotations(document.annotations, weighting, threshold, expansion_threshold, expansion_relatedness, expanded_weighting)))                                
        random.seed("www.itec-sde.net")
        random.shuffle(documents_training)
        documents_training = documents_training[0:num_documents_for_training]


    #### TEST ####                         
    if num_documents_for_testing == 0:                
        print "Obteniendo documents de test de todo el corpus " + corpus                                                                                                 
        for document in Session.query(Document).filter(Document.cgisplit == "test"):
            if weighting == "milne":
                documents_test.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))
            elif weighting == "binary":
                documents_test.append((document.id, document.original_category, [(annotation.name,1) for annotation in document.annotations if annotation.weight >= threshold]))
        random.seed("www.itec-sde.net")
        random.shuffle(documents_test)
    else:
        print "Obteniendo documents de test de una parte del corpus " + corpus
        num_test = 0
        for document in Session.query(Document).filter(Document.cgisplit == "test"):
            if weighting == "milne":                                                         
                documents_test.append((document.id, document.original_category, [(annotation.name,annotation.weight) for annotation in document.annotations if annotation.weight >= threshold]))
            elif weighting == "binary":
                documents_test.append((document.id, document.original_category, [(annotation.name,1) for annotation in document.annotations if annotation.weight >= threshold]))
        random.seed("www.itec-sde.net")
        random.shuffle(documents_test)   
        documents_test = documents_test[0:num_documents_for_test]
                   
    return documents_train, documents_test

def get_documents_from_multilabel_database_bow(corpus, num_documents_for_training,num_documents_for_test = 0):
    print "--- METODO: get_documents_from_multilabel_database_bow() ---"
    
    stemmer = PorterStemmer()
    tokenizer =  RegexpTokenizer(r'\w+')
    
    documents_training = []
    documents_test = []     
    ####### TRAINING #######             
    if num_documents_for_training == 0:
        print "Obteniendo documents de training de todo el corpus" + corpus            
        for document in Session.query(Document).filter(Document.cgisplit == "train"):    
            documents_training.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description.decode(encoding="ISO-8859-1"))] ))
        random.seed("www.itec-sde.net")
        random.shuffle(documents_training)
    else:
        print "Obteniendo documents de training de una parte del corpus " + corpus                                                                                                
        for document in Session.query(Document).filter(Document.cgisplit == "train"):                    
            documents_training.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description.decode(encoding="ISO-8859-1"))]))                    
        random.seed("www.itec-sde.net")
        random.shuffle(documents_training)
        documents_training = documents_training[0:num_documents_for_training]
     
    
    ######## TEST ##################
    if num_documents_for_test == 0:
        print "Obteniendo documents de test de todo el corpus " + corpus                                                                                                 
        for document in Session.query(Document).filter(Document.cgisplit == "test"):
            documents_test.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description.decode(encoding="ISO-8859-1"))]))
        random.seed("www.itec-sde.net")
        random.shuffle(documents_test)
    else:
        print "Obteniendo documents de test de una parte del corpus " + corpus                                                                                                
        for document in Session.query(Document).filter(Document.cgisplit == "test"):
            documents_test.append((document.id, document.original_category, [stemmer.stem(word_stem) for word_stem in tokenizer.tokenize(document.description.decode(encoding="ISO-8859-1"))]))                    
        random.seed("www.itec-sde.net")
        random.shuffle(documents_test)   
        documents_test = documents_test[0:num_documents_for_test]
        
    return documents_training, documents_test


'''
    Devuelve la lista de palabras unicas de todos los documentos que se le pasan y 
    el peso total de cada una de ellas
'''  

def get_unique_words_boc(documents):
    print "--- METODO: get_unique_words() ---"
    words_with_duplicates = []
    words_features = {}
    
    for (id, cat, annotations) in documents:
        for (annotation_name, annotation_weight) in annotations:
            words_with_duplicates.append(annotation_name)
    
    words_unique = list(set(words_with_duplicates))
    
    for word in words_unique:
        for (id, cat, annotations) in documents:
            for (annotation_name, annotation_weight) in annotations:
                if annotation_name == word:
                    if words_features.has_key(word):
                        words_features[word] += annotation_weight
                    else:
                        words_features[word] = annotation_weight
   
    return words_features
'''
    print "WORD_FEATURES : %d" % len(words_features)
    print words_features
    print "Nueva lista duplicado : %d" % len(words_with_duplicates)
    print words_with_duplicates    
    print "Nueva lista unica : %d" % len(words_unique)
    print words_unique
'''


'''
    Devuelve la lista de palabras unicas de todos los documentos que se le pasan y 
    el numero de veces que aparecen en total entre todos los documentos
'''
def get_unique_words_bow(documents):
    print "--- METODO: get_unique_words_bow() ---"
    words_with_duplicates = []
    words_features = {}    
    for (id, cat, list_words) in documents:
        for (word) in list_words:
            words_with_duplicates.append(word)
        
    words_unique = list(set(words_with_duplicates))
    #Hack para que valgan otros metodos y aprovecharlos
    for word in words_unique:
        words_features[word] = 1    
    return words_features


'''
    Transforma el documento en una lista donde se ponen los pesos en un documento particular de la lista de palabras del corpus de training 
'''    
def transform_document_in_vector(annotations, words_features,corpus):
    document_vector = []    
    for annotation_name,total_weight in words_features.iteritems():
        if "boc" in corpus:        
            document_vector.append(get_annotation_weight(annotation_name,annotations)) #/float(total_weight))
        elif "bow" in corpus:
            document_vector.append(annotations.count(annotation_name)) #/float(total_weight)                
    return document_vector


'''
    Transforma el documento en un diccionario donde se ponen los pares: palabra a la que hace referencia y su peso peso en el documento particular. 
    Esto de toda la lista de palabras del corpus de training.  
''' 
def transform_document_in_dict(annotations, words_features,corpus):
    document_features = {}    
    for annotation_name,total_weight in words_features.iteritems():
        if "boc" in corpus:
            document_features[annotation_name] =  get_annotation_weight(annotation_name,annotations) #/float(total_weight)
        elif "bow" in corpus:
            document_features[annotation_name] =  annotations.count(annotation_name) #/float(total_weight)    
    return document_features



def get_annotation_weight(annotation_name, annotations):
    for (name, weight) in annotations:
        if name == annotation_name:
            return weight
    return 0

       

def print_experiment_details(corpus, threshold,number_of_documents_for_training, number_of_documents_for_testing, classify_method,tfidf,stemming,smoothing, weighting, expansion_threshold, expansion_relatedness, expanded_weighting, neighbors, metric):
    print "---- EXPERIMENT DETAILS ----"
    print "CORPUS: " + corpus.replace("bow_","").replace("boc_","")
    if "bow" in corpus:
        print "REPRESENTATION: BoW" 
    else:
        print "REPRESENTATION: BoC"    
    print "THRESHOLD: " + str(threshold)
    if "boc" in corpus:
        if expansion_threshold != 1 and expansion_relatedness != 1:
            print "EXPANSION THRESHOLD: " + str(expansion_threshold)
            print "EXPANSION RELATEDNESS: " + str(expansion_relatedness)
            print "EXPANDED WEIGHTING: " + str(expanded_weighting) 
                     
    print "CLASSIFY METHOD: " + classify_method
    print "METRIC: " + metric
    if tfidf == True:
        print "TFIDF: yes"                            
    else:
        print "TFIDF: no"
    if classify_method == "kneighbors":
	print "NEIGHBORS: " + str(neighbors)
    if "bow" in corpus:
        print "STEMMING: " + stemming
    else:
        print "STEMMING: -"
    print "SMOOTHING: " + str(smoothing)
    print "WEIGHTING: " + weighting 
 
    if number_of_documents_for_training == 0:
        print "#TRAIN: all"       
    else:
        print "#TRAIN: " + str(number_of_documents_for_training)
        
    if number_of_documents_for_testing == 0:
        print "#TEST: all"
    else:
        print "#TEST: " + str(number_of_documents_for_testing)
    print

    
    
def get_experiment_results(corpus, threshold,number_of_documents_for_training, number_of_documents_for_testing, classify_method,tfidf,stemming,smoothing, weighting, expansion_threshold, expansion_relatedness, f1_score, expanded_weighting, kbest, n_neighbors, metric):
    c_corpus = corpus.replace("bow_","").replace("boc_","")
    if "bow" in corpus:
        c_representation = "BoW"
    else:
        c_representation = "BoC"    
    
    if "boc" in corpus and expansion_threshold != 1 and expansion_relatedness != 1:
        
        c_expansion_threshold = expansion_threshold
        c_expansion_relatedness = expansion_relatedness
        c_expanded_weighting = expanded_weighting
    else:
        c_expansion_threshold = None
        c_expansion_relatedness = None
        c_expanded_weighting = None
           
    content = {'corpus': c_corpus, 'representation': c_representation, 'threshold': threshold, 'expansion_threshold': c_expansion_threshold, 'expansion_relatedness': c_expansion_relatedness,'classify_method': classify_method, 'tfidf': tfidf, 'stemming': stemming, 'smoothing': smoothing, 'weighting': weighting, 'train': number_of_documents_for_training, 'test': number_of_documents_for_testing, 'f1_score': f1_score, 'expanded_weighting': c_expanded_weighting , 'KBest': kbest, 'n_neighbors': n_neighbors, 'metric': metric}
    
    return content
    
def get_experiment_multilabel_results(corpus, threshold,number_of_documents_for_training, number_of_documents_for_testing, classify_method,tfidf,stemming,smoothing, weighting, expansion_threshold, expansion_relatedness, f1_score, expanded_weighting, kbest, n_neighbors, metric, hamming_loss, accuracy, precision, recall):
    c_corpus = corpus.replace("bow_","").replace("boc_","")
    if "bow" in corpus:
        c_representation = "BoW"
    else:
        c_representation = "BoC"    
    
    if "boc" in corpus and expansion_threshold != 1 and expansion_relatedness != 1:
        
        c_expansion_threshold = expansion_threshold
        c_expansion_relatedness = expansion_relatedness
        c_expanded_weighting = expanded_weighting
    else:
        c_expansion_threshold = None
        c_expansion_relatedness = None
        c_expanded_weighting = None
           
    content = {'corpus': c_corpus, 'representation': c_representation, 'threshold': threshold, 'expansion_threshold': c_expansion_threshold, 'expansion_relatedness': c_expansion_relatedness,'classify_method': classify_method, 'tfidf': tfidf, 'stemming': stemming, 'smoothing': smoothing, 'weighting': weighting, 'train': number_of_documents_for_training, 'test': number_of_documents_for_testing, 'f1_score': f1_score, 'expanded_weighting': c_expanded_weighting , 'KBest': kbest, 'n_neighbors': n_neighbors, 'metric': metric, 'hamming_loss': hamming_loss, 'accuracy': accuracy, 'precision': precision, 'recall': recall}
    
    return content
    
    
    
                            
