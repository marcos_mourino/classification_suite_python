import util_classify
import classify_methods


from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import hamming_loss

import sys
import argparse
import json
import time
import os

class Logger(object):
    def __init__(self,filename):
        self.terminal = sys.stdout
        #self.log = open("/media/almacen/Dropbox/Concept-Based_Paradigm/Classification/Results/" + filename, "w")
        self.log = open("Results/" + filename, "w")
    
    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
        self.log.flush()
         
    def close(self):
        self.log.close() 

# len(sys.argv)


#### Parametros disponible para iterar #### 
corpus_availables = ["boc_reuters_27000","bow_reuters_27000","boc_ohsumed","bow_ohsumed","boc_ohsumed_expanded","boc_20_newsgroup","bow_20_newsgroup","boc_ieee","bow_ieee","boc_ieee_expanded","boc_20_newsgroup_expanded","boc_reuters_21578","bow_reuters_21578","boc_reuters_21578_expanded", "bow_oercommons"]
classify_methods_availables = ["mbayes", "kneighbors","multilabel"]
threshold_availables = [0.01, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90]
weighting_availables = ["milne", "binary"] #centrodid
metric_availables = ["cosine","jaccard","braycurtis","mahalanobis"]
 

parser = argparse.ArgumentParser(description = 'Clasificacion de contenidos')
parser.add_argument('-corpus', dest = 'corpus', default = "all", help = "Si no se pone nada por defecto hace todos los corpus disponibles", choices =  ['boc_reuters_27000','bow_reuters_27000','boc_ohsumed','bow_ohsumed','boc_ohsumed_expanded','boc_20_newsgroup','bow_20_newsgroup','boc_ieee','bow_ieee','boc_ieee_expanded','boc_20_newsgroup_expanded','boc_reuters_21578','bow_reuters_21578','boc_reuters_21578_expanded','bow_oercommons'])
parser.add_argument('-method', dest = 'classify_method', default = "mbayes", help = "Metodo de clasificacion. Por defecto mbayes", choices =  ["all", "mbayes", "kneighbors","multilabel"])
parser.add_argument('-threshold', dest = 'threshold', default = 0.01, type = float, help = "Umbral a partir del cual se tienen en cuenta las anotationes de un documento. Por defecto 0.01", choices =  threshold_availables)
parser.add_argument('-test', dest = 'test', default = 0, type = int, help = "Numero de documentos para test. Si no se pone nada se utiliza lo que venga en el corpus por defecto")
parser.add_argument('-train', dest = 'train', default = 0, type = int ,help = "Numero de documentos para training. Si no se pone nada se utiliza lo que venga en el corpus por defecto")
parser.add_argument('-weighting',  dest ='weighting', default = "milne", help = "Peso que utilizamos para calcular probabilidades. Por defecto Milne.", choices =  ['all', 'milne','centroid', 'binary'])
parser.add_argument('-smoothing',  dest ='smoothing', default = 0.001, type = float,  help = "Smoothing para el mbayes. Es el parametro alpha que se le mete a la clase MultinomialNB.")

parser.add_argument('-exp_threshold',  dest ='expansion_threshold', default = 1, type = float,  help = "")
parser.add_argument('-exp_relatedness',  dest ='expansion_relatedness', default = 1, type = float,  help = "")
parser.add_argument('-exp_weighting', dest ='expanded_weighting', default = 0.1, type = float, help = "")
parser.add_argument('-kbest', dest ='kbest', default = 3000, type = int, help = "")

parser.add_argument('-n_neighbors', dest="n_neighbors", default = 10, type=int, help="")

parser.add_argument('-metric', dest="metric", default = "cosine", help="", choices = ['cosine','jaccard','braycurtis','mahalanobis'])

parser.add_argument('-destination_folder', dest="destination_folder", help="Nombre de la carpeta de destino donde se guardan los .json del experimento")

args = parser.parse_args()

##------------------------
if args.corpus == 'all':
    array_corpus = corpus_availables
else:
    array_corpus = [args.corpus]
##------------------------
if args.classify_method == 'all':
    array_classify_methods = classify_methods_availables
else:
    array_classify_methods = [args.classify_method]
##------------------------
array_threshold = [args.threshold]

##------------------------
if args.weighting == 'all':
    array_weighting = weighting_availables
else:
    array_weighting = [args.weighting]
##------------------------
smoothing = args.smoothing
###
expansion_threshold = args.expansion_threshold
expansion_relatedness = args.expansion_relatedness
expanded_weighting = args.expanded_weighting
kbest = args.kbest
n_neighbors = args.n_neighbors
metric = args.metric
destination_folder = args.destination_folder

##### PARAMETROS DE ENTRADA ####
classify_method = array_classify_methods[0]
tfidf = False #Aun no operativo
stemming = "porter" #Aun no operativo
weighting = array_weighting[0]
#json_path = "/media/almacen/Dropbox/Concept-Based_Paradigm/Classification/Results/json/"

if args.classify_method == "mbayes":
	json_path = "Results/json/BAYES/" + destination_folder + "/"
elif args.classify_method == "kneighbors":
	json_path =  "Results/json/KNN/" + destination_folder + "/"
elif args.classify_method == "multilabel":
    	json_path =  "Results/json/Multilabel/" + destination_folder + "/"

if os.path.exists(json_path) == False:
	os.mkdir(json_path)

##### FIN PARAMETROS ENTRADA ####


#### PROGRAMA PRINCIPAL ####
for corpus in array_corpus:
    
    ##------------------------
    if args.train == 0 and args.test == 0:
        if 'reuters' in corpus:
            array_number_of_documents_for_training = [2400]
            array_number_of_documents_for_testing = [500]
        elif 'ieee' in corpus:
            array_number_of_documents_for_training = [550]
            array_number_of_documents_for_testing = [140]
        else:
            array_number_of_documents_for_training = [args.train]
            array_number_of_documents_for_testing = [args.test]        
            
    else:       
        array_number_of_documents_for_training = [args.train]
        array_number_of_documents_for_testing = [args.test]
    
    number_of_documents_for_testing = array_number_of_documents_for_testing[0]


    for number_of_documents_for_training in array_number_of_documents_for_training:
        for threshold in array_threshold:
                                   
            
            ### Creamos fichero donde se guardaran los resultados del experimento y hacemos que stdout salga por pantalla y ficheros            
            if number_of_documents_for_training == 0 and number_of_documents_for_testing == 0:
                filename = corpus + "_th_"+ (str(threshold)).replace("0.","") + "_all_" + classify_method + ".txt"
            else:
                filename = corpus + "_th_"+ (str(threshold)).replace("0.","") + "_" + str(number_of_documents_for_training) + "_" + str(number_of_documents_for_testing) + "_" + classify_method + ".txt"
                                    
            old_stdout = sys.stdout
            sys.stdout = Logger(filename)
            
            
            ## Pintamos los detalles del experimento
            util_classify.print_experiment_details(corpus, threshold,number_of_documents_for_training, number_of_documents_for_testing, classify_method,tfidf,stemming,smoothing, weighting, expansion_threshold, expansion_relatedness, expanded_weighting, n_neighbors, metric)                       
           
            ## Establecemos sesion con la base de datos
            util_classify.set_database_session(corpus)            
            
            #### Obtenemos documentos de training y test por separado y listados de palabras de training unicas con su frecuencia ####            
            print "Obteniendo documents de training y de testing..."
            if args.classify_method == "multilabel":
                if "boc" in corpus:
                    documents_training, documents_test = util_classify.get_documents_from_multilabel_database_boc(corpus, threshold, weighting, expansion_threshold, expansion_relatedness, number_of_documents_for_training, expanded_weighting, number_of_documents_for_testing)
                    words_features = util_classify.get_unique_words_boc(documents_training)            
                elif "bow" in corpus:
                    documents_training, documents_test = util_classify.get_documents_from_multilabel_database_bow(corpus,number_of_documents_for_training,number_of_documents_for_testing)
                    words_features = util_classify.get_unique_words_bow(documents_training)       
            else:
                if "boc" in corpus:
                    documents_training, documents_test = util_classify.get_documents_from_database_boc(corpus, threshold, weighting, expansion_threshold, expansion_relatedness, number_of_documents_for_training, expanded_weighting, number_of_documents_for_testing)
                    words_features = util_classify.get_unique_words_boc(documents_training)            
                elif "bow" in corpus:
                    documents_training, documents_test = util_classify.get_documents_from_database_bow(corpus,number_of_documents_for_training,number_of_documents_for_testing)
                    words_features = util_classify.get_unique_words_bow(documents_training)                
            
            
            #### Pasamos el clasificador y pintamos el report de la clasificacion                         
            if classify_method == "all_classifiers" or classify_method == "kneighbors":
                #### K-neighbours ####
                original_categories, estimated_categories = classify_methods.kneighbors(corpus,documents_training, documents_test, words_features,n_neighbors,metric)
                print classification_report(original_categories,estimated_categories,target_names=util_classify.get_categories(corpus))
                f1_score_result = f1_score(original_categories,estimated_categories, pos_label= None, average = 'macro')
            
            if classify_method == "all_classifiers" or classify_method == "mbayes":
                #### Multinomial bayes ####
                if tfidf == True:
                    #print kbest
                    original_categories, estimated_categories = classify_methods.multinomial_bayes_nltk_wraper(corpus,documents_training, documents_test, words_features, smoothing, kbest)
                else:
                    original_categories, estimated_categories = classify_methods.multinomial_bayes_sklearn(corpus,documents_training, documents_test, words_features, smoothing)                                    
                print classification_report(original_categories,estimated_categories,target_names=util_classify.get_categories(corpus))
                f1_score_result = f1_score(original_categories,estimated_categories, pos_label= None, average = 'macro')
                
            if classify_method == "all_classifiers" or classify_method == "multilabel":
                #Multilabel
                ground_truth_vector_categories, prediction = classify_methods.multilabel(corpus,documents_training, documents_test, words_features,smoothing)                
                print "Metricas:"
                print "----------------"
                print "Hamming Loss:"
                hamming_loss = hamming_loss(ground_truth_vector_categories,prediction)
                print hamming_loss
                print 
                print "Accuracy:"
                accuracy = accuracy_score(ground_truth_vector_categories,prediction)
                print accuracy
                print
                print "Precision:"
                precision = precision_score(ground_truth_vector_categories,prediction)
                print precision
                print
                print "Recall:"
                recall = recall_score(ground_truth_vector_categories,prediction)
                print recall
                print
                print "Classification Report:"
                print classification_report(ground_truth_vector_categories,prediction,target_names=util_classify.get_multiple_categories(corpus))
                print "F1-score"
                f1_score_result = f1_score(ground_truth_vector_categories,prediction, pos_label= None, average = 'macro')
                print f1_score_result
                print "----------------"

                
            #f1_score_result = 0.5
            print "F1_SCORE (Macro): " +str(f1_score_result)
            if classify_method == "multilabel":
                filename = corpus + "_th_"+ (str(threshold)).replace("0.","") + "_" + str(number_of_documents_for_training) + "_" + str(number_of_documents_for_testing) + "_" + classify_method + "_" + str(n_neighbors) + "_neighbors" +str(int(time.time()))+  ".json"
                with open(json_path + filename, "w") as outfile:
                    json.dump(util_classify.get_experiment_multilabel_results(corpus, threshold,number_of_documents_for_training, number_of_documents_for_testing, classify_method,tfidf,stemming,smoothing, weighting, expansion_threshold, expansion_relatedness, f1_score_result, expanded_weighting, kbest, n_neighbors, metric, hamming_loss, accuracy, precision, recall), outfile)
            else:
                filename = corpus + "_th_"+ (str(threshold)).replace("0.","") + "_" + str(number_of_documents_for_training) + "_" + str(number_of_documents_for_testing) + "_" + classify_method + "_" + str(n_neighbors) + "_neighbors" +str(int(time.time()))+  ".json"
                with open(json_path + filename, "w") as outfile:
                    json.dump(util_classify.get_experiment_results(corpus, threshold,number_of_documents_for_training, number_of_documents_for_testing, classify_method,tfidf,stemming,smoothing, weighting, expansion_threshold, expansion_relatedness, f1_score_result, expanded_weighting, kbest, n_neighbors, metric), outfile)
                                
            
            print "---- Fin ejecucion ----"            
            sys.stdout.close()
            sys.stdout = old_stdout
        


