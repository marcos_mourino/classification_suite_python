python classify.py -corpus boc_ieee -method mbayes -train 5 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 10 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 15 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 20 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 25 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 30 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 40 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 50 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 75 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 100 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 150 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 300 -test 138
python classify.py -corpus boc_ieee -method mbayes -train 553 -test 138

python classify.py -corpus bow_ieee -method mbayes -train 5 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 10 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 15 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 20 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 25 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 30 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 40 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 50 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 75 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 100 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 150 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 300 -test 138
python classify.py -corpus bow_ieee -method mbayes -train 553 -test 138

python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.9 -exp_relatedness 0.9 -test 138

python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138

python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.8 -exp_relatedness 0.9 -test 138

python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138
python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.9 -exp_relatedness 0.8 -test 138

python display_graphic_ieee_various_expansions.py experiment_ieee_with_various_expanded_thresholds



