import util_classify
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import hamming_loss

from nltk.util import LazyMap

from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_selection import SelectKBest, chi2

import numpy as np
from nltk.classify import apply_features

from sklearn import datasets
from sklearn.datasets import load_iris
from sklearn.datasets import make_multilabel_classification
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import LabelBinarizer
from sklearn.decomposition import PCA
from sklearn.pls import CCA
from sklearn.feature_extraction.text import TfidfVectorizer

WordFeatures = None



def kneighbors(corpus,documents_training, documents_test, words_features,n_neighbors,metric):
    print
    print "----- ALGORITMO KNeighbours ------"
    print "Creando Vectores de training..."
    array_vector_training = []
    array_categories = []
    for (id ,original_category, annotations) in documents_training:
        array_vector_training.append(util_classify.transform_document_in_vector(annotations,words_features,corpus))
        array_categories.append(util_classify.get_categories(corpus).index(original_category))    
        
    print "Entrenando el algoritmo..."
    X = array_vector_training
    y = array_categories
    if metric == "cosine":
    	neigh = KNeighborsClassifier(n_neighbors=n_neighbors, metric='pyfunc',func=util_classify.cosine_measure)
    elif metric == "jaccard":
	neigh = KNeighborsClassifier(n_neighbors=n_neighbors, metric='pyfunc',func=util_classify.jaccard_measure)
    elif metric == "braycurtis":
	neigh = KNeighborsClassifier(n_neighbors=n_neighbors, metric='pyfunc',func=util_classify.braycurtis_measure)
    elif metric == "mahalanobis":
	neigh = KNeighborsClassifier(n_neighbors=n_neighbors, metric='pyfunc',func=util_classify.mahalanobis_measure)
    neigh.fit(X, y) 
        
    
    print "Calculando metricas ..."
    estimated_categories = []
    original_categories = []
    for (id ,original_category, annotations) in documents_test: 
        vector = util_classify.transform_document_in_vector(annotations,words_features,corpus)
        cat_classify = neigh.predict([vector])        
        estimated_categories.append(util_classify.get_categories(corpus)[cat_classify])
        original_categories.append(original_category)       
        
    return original_categories, estimated_categories




'''
    Multinomial Naive Bayes
    Utilizando el wraper de la NLTK SklearnClassifier
    Puede tener problemas de memoria si conjunto de datos muy grande 
'''
def multinomial_bayes_nltk_wraper(corpus, documents_training, documents_test, words_features,smoothing,kbest):             
    print
    print "----- ALGORITMO Multinomial Bayes con wraper nltk ------"
    print "Creando Vectores features de training..."
    array_features_training = []
    for (id ,original_category, annotations) in documents_training:
        array_features_training.append((util_classify.transform_document_in_dict(annotations,words_features,corpus),original_category))                                                       
        
    #array_features_training = apply_features(extract_document_features,documents_training)
    
        
    print "Entrenando algoritmo..."
    #('chi2', SelectKBest(chi2, k=3000)),
    
    if kbest == 0:
        kbest = "all"

    pipeline = Pipeline([('chi2', SelectKBest(chi2, k=kbest)),('tfidf', TfidfTransformer()),                         
                         ('nb', MultinomialNB(alpha=smoothing))])
    '''
    pipeline = Pipeline([('nb', MultinomialNB(alpha=smoothing))])
    '''
    classifier = SklearnClassifier(pipeline)
    classifier.train(array_features_training)

    
    print "Calculando metricas ..."
    categories = util_classify.get_categories(corpus)
    estimated_categories = []
    original_categories = []               
    
    for (id ,cat_original, annotations) in documents_test:
        cat_estimated = classifier.classify((util_classify.transform_document_in_dict(annotations,words_features,corpus)))
        estimated_categories.append(categories.index(cat_estimated))
        original_categories.append(categories.index(cat_original))
    return original_categories, estimated_categories
        



'''
    Multinomial Naive Bayes
    Utilizando solo libreria MultinomialNB de sklearn 
    Entrenado por partes para evitar problemas de memoria 
'''
def multinomial_bayes_sklearn(corpus, documents_training, documents_test, words_features,smoothing):         
    print "----- ALGORITMO Multinomial Bayes con sklearn puro ------"
    categories = util_classify.get_categories(corpus)    
    classifier = MultinomialNB(alpha = smoothing)
          
    '''
    print "Entrenando algoritmo por completo..."
    X_train_features = []
    y_train_categories = []
    ##### Entrenandolo de golpe
    for (id ,original_category, annotations) in documents_training:        
        X_train_features.append(util_classify.transform_document_in_vector(annotations,words_features,corpus)) 
        y_train_categories.append(original_category)
    
    classifier.fit(np.array(X_train_features), np.array(y_train_categories))    
    '''
    
    ##### Entrenandolo por partes
    print "Entrenando algoritmo por partes..." 
    First = True        
    for (id ,original_category, annotations) in documents_training:
        if First == True:            
            classifier.partial_fit(np.array(util_classify.transform_document_in_vector(annotations,words_features,corpus)),  np.array([original_category]),classes = categories)
            First = False
        else:
            classifier.partial_fit(np.array(util_classify.transform_document_in_vector(annotations,words_features,corpus)),  np.array([original_category]))
                      
    print "Calculando metricas ..."
    estimated_categories = []
    original_categories = []               
    
    for (id ,cat_original, annotations) in documents_test:
        cat_estimated = classifier.predict(np.array((util_classify.transform_document_in_vector(annotations,words_features,corpus))))
        estimated_categories.append(categories.index(cat_estimated))
        original_categories.append(categories.index(cat_original))
    return original_categories, estimated_categories

def multilabel(corpus, documents_training, documents_test, words_features,smoothing):

    print "----- ALGORITMO Multilabel ------"
    print "Creando Vectores de training..."
    print "Creando Vectores features de training..."
    
    first_time = 0
    first_time_categories = 0
    for (id ,original_category, annotations) in documents_training:
        vector = util_classify.transform_document_in_vector(annotations,words_features,corpus)
        vector = np.array(vector)
        if first_time == 0:
            array_vector_training = vector
            first_time = 1
        else:
            array_vector_training = np.vstack((array_vector_training,vector))
        #print "original_category"
        #print original_category
        original_categories = [x.strip() for x in original_category.split(',')]
        #print "original_categories"
        #print original_categories

        #entre aqui 
        #original_categories = []
        #original_categories.append(original_category)
        #y aqui hay que borrarlo y descomentar la linea que esta comentada antes de este comentario

        vector_categories = np.zeros(21)
        for category in original_categories:
            #vector_categories.append(util_classify.get_multiple_categories(corpus).index(category))
            vector_categories[util_classify.get_multiple_categories(corpus).index(category)] = 1
        vector_categories = np.array(vector_categories)
        if first_time_categories == 0:
            array_vector_categories = vector_categories
            first_time_categories = 1
        else:
            array_vector_categories = np.vstack((array_vector_categories,vector_categories))
        #print array_vector_categories

    #print "array_vector_training"
    #print array_vector_training
    #print "array_vector_categories"
    #print array_vector_categories
    
    print "Entrenando clasificador Multilabel"

    classif = OneVsRestClassifier(SVC(kernel='linear'))
    classif.fit(array_vector_training, array_vector_categories)

    #print "Clasificando los elementos utilizados para entrenamiento para ver que las clasifica bien"

    #prediction_classif = classif.predict(array_vector_training)

    #print prediction_classif
    
    print "Clasificando en categorias los documentos de test (Prediccion)"
    estimated_categories = []
    original_categories = []
    first_time = 0
    first_time_categories = 0
    for (id ,original_category, annotations) in documents_test: 
        vector = util_classify.transform_document_in_vector(annotations,words_features,corpus)
        vector = np.array(vector)
        if first_time == 0:
            array_vector_test = vector
            first_time = 1
        else:
            array_vector_test = np.vstack((array_vector_test,vector))

        #print "train"
        #print array_vector_training
        #print "test"
        #print array_vector_test

        #A partir de aqui calculamos el vector de categorias originales para los documentos de test para utilizar
        #como ground truth a la hora de realizar las medidas
        
        original_categories = [x.strip() for x in original_category.split(',')]

        #entre aqui
        #original_categories = []
        #original_categories.append(original_category)
        #y aqui hay que borrarlo y descomentar la linea que esta comentada antes de este comentario

        vector_categories = np.zeros(21)
        for category in original_categories:
            #vector_categories.append(util_classify.get_multiple_categories(corpus).index(category))
            vector_categories[util_classify.get_multiple_categories(corpus).index(category)] = 1
        vector_categories = np.array(vector_categories)
        if first_time_categories == 0:
            ground_truth_vector_categories = vector_categories
            first_time_categories = 1
        else:
            ground_truth_vector_categories = np.vstack((ground_truth_vector_categories,vector_categories))
        
        #hasta aqui

    prediction = classif.predict(array_vector_test)    

    #print id
    #print prediction



    #estimated_categories.append(util_classify.get_categories(corpus)[cat_classify])
    #original_categories.append(original_category)       
        
    return ground_truth_vector_categories, prediction

    '''
    X, Y = make_multilabel_classification(n_samples = 46, n_features = 976, n_classes = 2, n_labels = 1, allow_unlabeled=False, return_indicator=True, random_state = 1)

    print "X"
    print X
    print "Y"
    print Y
    
    classif = OneVsRestClassifier(SVC(kernel='linear'))
    classif.fit(X, Y)

    prediction = classif.predict(X)

    print "prediction"
    print prediction

    print (prediction == Y)   

    '''  


#################################################################################
################ PRUBAS VARIAS NO FUNCIONAL DE AQUI PARA ABAJO ##################
#################################################################################        

def extract_document_features(document,words_features):
    id ,cat_original, annotations = document
    corpus = "boc"
    document_features = {}  
    for annotation_name,total_weight in words_features.iteritems():
        if "boc" in corpus:
            document_features[annotation_name] =  util_classify.get_annotation_weight(annotation_name,annotations) #/float(total_weight)
        elif "bow" in corpus:
            document_features[annotation_name] =  annotations.count(annotation_name) #/float(total_weight)    
    return (document_features, cat_original)
   
                
def multinomial_bayes3(corpus, documents_training, documents_test, words_features,smoothing):             
    print
    print "----- ALGORITMO Multinomial Bayes ------"
    print "Creando Vectores features de training..."
        
    print documents_training
    #array_features_training = apply_features(extract_document_features,documents_training,words_features)
    array_features_training = list(LazyMap(extract_document_features, documents_training, words_features))
    
    
                
    #vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,stop_words='english')
    #X_train = vectorizer.fit_transform(data_train.data)
                    
    #array_features_training.append((util_classify.transform_document_in_dict(annotations,words_features,corpus),original_category))
    #y_train_categories = np.array([original_category])
    #X_train_features = np.array(util_classify.transform_document_in_vector(annotations,words_features))
    #tfidf_transformer = TfidfTransformer()        
    #X_train_tfidf_transformer = tfidf_transformer.fit_transform(X_train_features)
    
    print "Entrenando algoritmo..."
    #('chi2', SelectKBest(chi2, k=3000)),
    pipeline = Pipeline([('tfidf', TfidfTransformer()),                         
                         ('nb', MultinomialNB(alpha=smoothing))])
    classifier = SklearnClassifier(pipeline)
    classifier.train(array_features_training)

    
    print "Calculando metricas ..."
    categories = util_classify.get_categories(corpus)
    estimated_categories = []
    original_categories = []               
    
    for (id ,cat_original, annotations) in documents_test:
        cat_estimated = classifier.classify((util_classify.transform_document_in_dict(annotations,words_features,corpus)))
        estimated_categories.append(categories.index(cat_estimated))
        original_categories.append(categories.index(cat_original))
    return original_categories, estimated_categories
        
        



