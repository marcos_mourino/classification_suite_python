#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000

#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000

#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000


#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 553 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000

# los que hubo que poner a all

#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 3000

#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 5000

#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 7000

#python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000
#python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 10000

# con todas las features

python classify.py -corpus boc_ieee_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0
python classify.py -corpus boc_ieee_expanded -method mbayes -train 300 -exp_threshold 0.8 -exp_relatedness 0.8 -test 138 -kbest 0




