---- EXPERIMENT DETAILS ----
CORPUS: ohsumed_expanded
REPRESENTATION: BoC
THRESHOLD: 0.01
EXPANSION THRESHOLD: 0.9
EXPANSION RELATEDNESS: 0.9
EXPANDED WEIGHTING: 0.1
CLASSIFY METHOD: kneighbors
TFIDF: no
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 5
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
Obteniendo documents de training de una parte del corpus boc_ohsumed_expanded
Obteniendo documents de test de todo el corpus boc_ohsumed_expanded
--- METODO: get_unique_words() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                                                     precision    recall  f1-score   support

                   Bacterial Infections and Mycoses       0.00      0.00      0.00        28
                                     Virus Diseases       0.08      0.36      0.13       102
                                 Parasitic Diseases       0.66      0.36      0.46       608
                                          Neoplasms       0.35      0.29      0.32       181
                           Musculoskeletal Diseases       0.40      0.13      0.20       369
                          Digestive System Diseases       0.22      0.31      0.26        77
                            Stomatognathic Diseases       0.43      0.68      0.53        81
                         Respiratory Tract Diseases       0.11      0.42      0.18       118
                      Otorhinolaryngologic Diseases       0.09      0.07      0.08       100
                            Nervous System Diseases       0.37      0.13      0.20       314
                                       Eye Diseases       0.18      0.30      0.23       140
                 Urologic and Male Genital Diseases       0.06      0.22      0.09        92
Female Genital Diseases and Pregnancy Complications       0.38      0.35      0.37       600
                            Cardiovascular Diseases       0.36      0.25      0.30       355
                       Hemic and Lymphatic Diseases       0.44      0.30      0.36       178
                Neonatal Diseases and Abnormalities       0.09      0.19      0.12        31
                Skin and Connective Tissue Diseases       0.05      0.48      0.09        29
                 Nutritional and Metabolic Diseases       0.37      0.02      0.03       956
                                 Endocrine Diseases       0.17      0.18      0.18       141
                               Immunologic Diseases       0.24      0.31      0.27       157
                  Disorders of Environmental Origin       0.05      0.39      0.10        36
                                    Animal Diseases       0.29      0.26      0.28       198
        Pathological Conditions, Signs and Symptoms       0.08      0.48      0.14        50

                                        avg / total       0.36      0.23      0.24      4941

F1_SCORE (Macro): 0.212431351265
---- Fin ejecucion ----
