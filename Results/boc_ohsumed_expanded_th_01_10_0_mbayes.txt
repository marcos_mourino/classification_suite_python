---- EXPERIMENT DETAILS ----
CORPUS: ohsumed_expanded
REPRESENTATION: BoC
THRESHOLD: 0.01
EXPANSION THRESHOLD: 0.9
EXPANSION RELATEDNESS: 0.8
EXPANDED WEIGHTING: 0.1
CLASSIFY METHOD: mbayes
TFIDF: no
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 10
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
Obteniendo documents de training de una parte del corpus boc_ohsumed_expanded
Obteniendo documents de test de todo el corpus boc_ohsumed_expanded
--- METODO: get_unique_words() ---
----- ALGORITMO Multinomial Bayes con sklearn puro ------
Entrenando algoritmo por partes...
Calculando metricas ...
                                                     precision    recall  f1-score   support

                   Bacterial Infections and Mycoses       0.16      0.40      0.23       102
                                     Virus Diseases       0.13      0.36      0.19        50
                                 Parasitic Diseases       0.19      0.52      0.28        29
                                          Neoplasms       0.49      0.35      0.41       600
                           Musculoskeletal Diseases       0.32      0.33      0.32       140
                          Digestive System Diseases       0.30      0.39      0.34       181
                            Stomatognathic Diseases       0.07      0.50      0.13        36
                         Respiratory Tract Diseases       0.23      0.30      0.26       141
                      Otorhinolaryngologic Diseases       0.09      0.39      0.15        31
                            Nervous System Diseases       0.34      0.39      0.36       355
                                       Eye Diseases       0.35      0.51      0.42        81
                 Urologic and Male Genital Diseases       0.32      0.34      0.33       198
Female Genital Diseases and Pregnancy Complications       0.40      0.35      0.37       118
                            Cardiovascular Diseases       0.72      0.37      0.49       608
                       Hemic and Lymphatic Diseases       0.12      0.26      0.17       100
                Neonatal Diseases and Abnormalities       0.06      0.24      0.10        92
                Skin and Connective Tissue Diseases       0.31      0.34      0.32       157
                 Nutritional and Metabolic Diseases       0.38      0.34      0.36       178
                                 Endocrine Diseases       0.10      0.36      0.16        77
                               Immunologic Diseases       0.29      0.13      0.18       314
                  Disorders of Environmental Origin       0.42      0.15      0.23       369
                                    Animal Diseases       0.04      0.36      0.07        28
        Pathological Conditions, Signs and Symptoms       0.22      0.06      0.09       956

                                        avg / total       0.35      0.27      0.28      4941

F1_SCORE (Macro): 0.258379951481
---- Fin ejecucion ----
