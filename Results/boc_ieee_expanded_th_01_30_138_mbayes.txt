---- EXPERIMENT DETAILS ----
CORPUS: ieee_expanded
REPRESENTATION: BoC
THRESHOLD: 0.01
EXPANSION THRESHOLD: 0.8
EXPANSION RELATEDNESS: 0.8
EXPANDED WEIGHTING: 0.1
CLASSIFY METHOD: mbayes
TFIDF: yes
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 30
#TEST: 138

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
--- METODO: get_unique_words() ---
0

----- ALGORITMO Multinomial Bayes con wraper nltk ------
Creando Vectores features de training...
Entrenando algoritmo...
Calculando metricas ...
                                                                      precision    recall  f1-score   support

                                                           Aerospace       0.25      0.39      0.31       138
                                                      Bioengineering       0.43      0.78      0.55       138
                        Communication  Networking .AND. Broadcasting       0.22      0.22      0.22       138
                         Components  Circuits  Devices .AND. Systems       0.53      0.68      0.59       138
                Computing .AND. Processing .LB.Hardware/Software.RB.       0.14      0.16      0.15       138
                     Engineered Materials  Dielectrics .AND. Plasmas       0.49      0.62      0.55       138
                                              Engineering Profession       0.34      0.20      0.25       138
                                Fields  Waves .AND. Electromagnetics       0.26      0.07      0.10       138
General Topics for Engineers .LB.Math  Science .AND. Engineering.RB.       0.38      0.36      0.37       138
                                                          Geoscience       0.41      0.20      0.27       138
                                      Photonics .AND. Electro-Optics       0.31      0.18      0.23       138
                          Power  Energy  .AND. Industry Applications       0.29      0.25      0.26       138
                                      Robotics .AND. Control Systems       0.02      0.02      0.02       138
                                    Signal Processing .AND. Analysis       0.50      0.43      0.47       138

                                                         avg / total       0.33      0.33      0.31      1932

F1_SCORE (Macro): 0.309667459681
---- Fin ejecucion ----
