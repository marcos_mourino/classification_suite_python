---- EXPERIMENT DETAILS ----
CORPUS: ieee_expanded
REPRESENTATION: BoC
THRESHOLD: 0.01
EXPANSION THRESHOLD: 0.8
EXPANSION RELATEDNESS: 0.8
EXPANDED WEIGHTING: 0.1
CLASSIFY METHOD: mbayes
TFIDF: yes
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 300
#TEST: 138

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
--- METODO: get_unique_words() ---
0

----- ALGORITMO Multinomial Bayes con wraper nltk ------
Creando Vectores features de training...
Entrenando algoritmo...
Calculando metricas ...
                                                                      precision    recall  f1-score   support

                                                           Aerospace       0.34      0.48      0.40       138
                                                      Bioengineering       0.54      0.83      0.65       138
                        Communication  Networking .AND. Broadcasting       0.41      0.24      0.30       138
                         Components  Circuits  Devices .AND. Systems       0.52      0.64      0.58       138
                Computing .AND. Processing .LB.Hardware/Software.RB.       0.40      0.51      0.45       138
                     Engineered Materials  Dielectrics .AND. Plasmas       0.57      0.75      0.65       138
                                              Engineering Profession       0.50      0.24      0.32       138
                                Fields  Waves .AND. Electromagnetics       0.48      0.18      0.26       138
General Topics for Engineers .LB.Math  Science .AND. Engineering.RB.       0.46      0.41      0.43       138
                                                          Geoscience       0.66      0.56      0.61       138
                                      Photonics .AND. Electro-Optics       0.55      0.70      0.62       138
                          Power  Energy  .AND. Industry Applications       0.36      0.39      0.37       138
                                      Robotics .AND. Control Systems       0.18      0.15      0.17       138
                                    Signal Processing .AND. Analysis       0.58      0.53      0.56       138

                                                         avg / total       0.47      0.47      0.45      1932

F1_SCORE (Macro): 0.454890980582
---- Fin ejecucion ----
