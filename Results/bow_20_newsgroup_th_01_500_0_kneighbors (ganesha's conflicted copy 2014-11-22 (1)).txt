---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: jaccard
TFIDF: no
NEIGHBORS: 50
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_20_newsgroup
Obteniendo documents de test de todo el corpus bow_20_newsgroup
--- METODO: get_unique_words_bow() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.34      0.60      0.43       232
      talk.politics.guns       0.23      0.45      0.31       389
      talk.politics.misc       0.34      0.43      0.38       394
             alt.atheism       0.40      0.32      0.36       401
               rec.autos       0.28      0.50      0.36       385
        rec.sport.hockey       0.50      0.36      0.42       395
         rec.motorcycles       0.81      0.41      0.54       325
               sci.space       0.43      0.51      0.47       396
   comp.sys.mac.hardware       0.80      0.32      0.46       154
            misc.forsale       0.67      0.24      0.36       153
   talk.politics.mideast       0.82      0.17      0.29       132
      talk.religion.misc       0.62      0.70      0.66       396
                 sci.med       0.26      0.03      0.05       177
               sci.crypt       0.45      0.37      0.41       396
 comp.os.ms-windows.misc       0.72      0.57      0.64       394
           comp.graphics       0.68      0.81      0.74       398
         sci.electronics       0.53      0.69      0.60       318
      rec.sport.baseball       0.83      0.31      0.45       311
          comp.windows.x       0.66      0.50      0.57       310
  soc.religion.christian       0.44      0.42      0.43       251

             avg / total       0.52      0.47      0.47      6307

F1_SCORE (Macro): 0.445380027293
---- Fin ejecucion ----
