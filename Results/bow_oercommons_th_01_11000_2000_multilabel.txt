---- EXPERIMENT DETAILS ----
CORPUS: oercommons
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: multilabel
METRIC: cosine
TFIDF: no
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 11000
#TEST: 2000

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_multilabel_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_oercommons
Obteniendo documents de test de una parte del corpus bow_oercommons
--- METODO: get_unique_words_bow() ---
----- ALGORITMO Multilabel ------
Creando Vectores de training...
Creando Vectores features de training...
Entrenando clasificador Multilabel
Clasificando en categorias los documentos de test (Prediccion)
Metricas:
----------------
Hamming Loss:
0.0906428571429

Accuracy:
0.316

Precision:
0.580427084937

Recall:
0.554621848739

Classification Report:
                            precision    recall  f1-score   support

                      Arts       0.55      0.50      0.53        62
                 Education       0.68      0.66      0.67       683
                  Business       0.55      0.38      0.44        32
                Humanities       0.69      0.66      0.67       244
Mathematics and Statistics       0.65      0.61      0.63       197
                   Physics       0.49      0.44      0.46       231
                Geoscience       0.58      0.57      0.57       267
 Computing and Information       0.35      0.34      0.35        50
                   Ecology       0.41      0.40      0.40       116
               Engineering       0.41      0.39      0.40        76
    Science and Technology       0.57      0.60      0.58       436
  Forestry and Agriculture       0.40      0.39      0.39       116
             Space Science       0.54      0.53      0.54       169
               Mathematics       0.52      0.49      0.50       172
              Life Science       0.69      0.69      0.69       541
                  Politics       0.46      0.34      0.39       106
                       Law       0.46      0.34      0.39       106
                Technology       0.37      0.37      0.37        90
           Social Sciences       0.60      0.57      0.58       467
                 Chemistry       0.54      0.51      0.53       255
                   History       0.46      0.34      0.39       106

               avg / total       0.58      0.55      0.57      4522

F1-score
0.499366110875
----------------
F1_SCORE (Macro): 0.499366110875
---- Fin ejecucion ----
