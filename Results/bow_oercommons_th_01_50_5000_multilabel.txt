---- EXPERIMENT DETAILS ----
CORPUS: oercommons
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: multilabel
METRIC: cosine
TFIDF: no
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 50
#TEST: 5000

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_multilabel_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_oercommons
Obteniendo documents de test de una parte del corpus bow_oercommons
--- METODO: get_unique_words_bow() ---
----- ALGORITMO Multilabel ------
Creando Vectores de training...
Creando Vectores features de training...
Entrenando clasificador Multilabel
Clasificando en categorias los documentos de test (Prediccion)
Metricas:
----------------
Hamming Loss:
0.120095238095

Accuracy:
0.073

Precision:
0.305414699985

Recall:
0.155213796782

Classification Report:
                            precision    recall  f1-score   support

                      Arts       0.12      0.05      0.07       154
                 Education       0.49      0.26      0.34      1683
                  Business       0.00      0.00      0.00        72
                Humanities       0.24      0.08      0.12       627
Mathematics and Statistics       0.21      0.03      0.06       497
                   Physics       0.50      0.01      0.02       595
                Geoscience       0.25      0.08      0.12       676
 Computing and Information       0.14      0.02      0.03       115
                   Ecology       0.40      0.01      0.01       304
               Engineering       0.06      0.01      0.02       186
    Science and Technology       0.30      0.16      0.21      1104
  Forestry and Agriculture       0.40      0.01      0.01       304
             Space Science       0.14      0.01      0.01       426
               Mathematics       0.17      0.01      0.01       404
              Life Science       0.40      0.49      0.44      1325
                  Politics       0.10      0.02      0.03       255
                       Law       0.10      0.02      0.03       255
                Technology       0.02      0.00      0.01       216
           Social Sciences       0.39      0.26      0.31      1167
                 Chemistry       0.13      0.02      0.04       629
                   History       0.10      0.02      0.03       255

               avg / total       0.31      0.16      0.18     11249

F1-score
0.0913941911509
----------------
F1_SCORE (Macro): 0.0913941911509
---- Fin ejecucion ----
