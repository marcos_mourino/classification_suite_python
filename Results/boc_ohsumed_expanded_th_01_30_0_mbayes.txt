---- EXPERIMENT DETAILS ----
CORPUS: ohsumed_expanded
REPRESENTATION: BoC
THRESHOLD: 0.01
EXPANSION THRESHOLD: 0.9
EXPANSION RELATEDNESS: 0.8
EXPANDED WEIGHTING: 0.1
CLASSIFY METHOD: mbayes
TFIDF: no
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 30
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
Obteniendo documents de training de una parte del corpus boc_ohsumed_expanded
Obteniendo documents de test de todo el corpus boc_ohsumed_expanded
--- METODO: get_unique_words() ---
----- ALGORITMO Multinomial Bayes con sklearn puro ------
Entrenando algoritmo por partes...
Calculando metricas ...
                                                     precision    recall  f1-score   support

                   Bacterial Infections and Mycoses       0.25      0.39      0.31       102
                                     Virus Diseases       0.14      0.42      0.21        50
                                 Parasitic Diseases       0.41      0.59      0.49        29
                                          Neoplasms       0.59      0.45      0.51       600
                           Musculoskeletal Diseases       0.33      0.48      0.39       140
                          Digestive System Diseases       0.37      0.47      0.41       181
                            Stomatognathic Diseases       0.13      0.56      0.22        36
                         Respiratory Tract Diseases       0.22      0.36      0.27       141
                      Otorhinolaryngologic Diseases       0.09      0.58      0.16        31
                            Nervous System Diseases       0.43      0.44      0.43       355
                                       Eye Diseases       0.40      0.62      0.49        81
                 Urologic and Male Genital Diseases       0.39      0.41      0.40       198
Female Genital Diseases and Pregnancy Complications       0.36      0.44      0.40       118
                            Cardiovascular Diseases       0.67      0.62      0.64       608
                       Hemic and Lymphatic Diseases       0.17      0.28      0.22       100
                Neonatal Diseases and Abnormalities       0.12      0.29      0.17        92
                Skin and Connective Tissue Diseases       0.47      0.39      0.43       157
                 Nutritional and Metabolic Diseases       0.38      0.31      0.35       178
                                 Endocrine Diseases       0.10      0.36      0.15        77
                               Immunologic Diseases       0.28      0.17      0.21       314
                  Disorders of Environmental Origin       0.35      0.27      0.30       369
                                    Animal Diseases       0.05      0.14      0.08        28
        Pathological Conditions, Signs and Symptoms       0.27      0.06      0.10       956

                                        avg / total       0.39      0.35      0.35      4941

F1_SCORE (Macro): 0.318714758721
---- Fin ejecucion ----
