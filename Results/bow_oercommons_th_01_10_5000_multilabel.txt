---- EXPERIMENT DETAILS ----
CORPUS: oercommons
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: multilabel
METRIC: cosine
TFIDF: no
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 10
#TEST: 5000

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_multilabel_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_oercommons
Obteniendo documents de test de una parte del corpus bow_oercommons
--- METODO: get_unique_words_bow() ---
----- ALGORITMO Multilabel ------
Creando Vectores de training...
Creando Vectores features de training...
Entrenando clasificador Multilabel
Clasificando en categorias los documentos de test (Prediccion)
Metricas:
----------------
Hamming Loss:
0.130980952381

Accuracy:
0.075

Precision:
0.10355471734

Recall:
0.0675615610276

Classification Report:
                            precision    recall  f1-score   support

                      Arts       0.00      0.00      0.00       154
                 Education       0.49      0.17      0.26      1683
                  Business       0.00      0.00      0.00        72
                Humanities       0.10      0.01      0.03       627
Mathematics and Statistics       0.15      0.68      0.24       497
                   Physics       0.00      0.00      0.00       595
                Geoscience       0.06      0.00      0.00       676
 Computing and Information       0.00      0.00      0.00       115
                   Ecology       0.00      0.00      0.00       304
               Engineering       0.02      0.01      0.01       186
    Science and Technology       0.15      0.11      0.12      1104
  Forestry and Agriculture       0.00      0.00      0.00       304
             Space Science       0.00      0.00      0.00       426
               Mathematics       0.00      0.00      0.00       404
              Life Science       0.00      0.00      0.00      1325
                  Politics       0.00      0.00      0.00       255
                       Law       0.00      0.00      0.00       255
                Technology       0.00      0.00      0.00       216
           Social Sciences       0.00      0.00      0.00      1167
                 Chemistry       0.00      0.00      0.00       629
                   History       0.00      0.00      0.00       255

               avg / total       0.10      0.07      0.06     11249

F1-score
0.031356951654
----------------
F1_SCORE (Macro): 0.031356951654
---- Fin ejecucion ----
