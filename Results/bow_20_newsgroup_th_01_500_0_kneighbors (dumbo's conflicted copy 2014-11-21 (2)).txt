---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: braycurtis
TFIDF: no
NEIGHBORS: 10
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_20_newsgroup
Obteniendo documents de test de todo el corpus bow_20_newsgroup
--- METODO: get_unique_words_bow() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.33      0.65      0.44       232
      talk.politics.guns       0.41      0.35      0.38       389
      talk.politics.misc       0.49      0.48      0.49       394
             alt.atheism       0.39      0.49      0.44       401
               rec.autos       0.35      0.55      0.43       385
        rec.sport.hockey       0.63      0.45      0.53       395
         rec.motorcycles       0.75      0.35      0.48       325
               sci.space       0.52      0.61      0.57       396
   comp.sys.mac.hardware       0.75      0.50      0.60       154
            misc.forsale       0.61      0.31      0.41       153
   talk.politics.mideast       0.62      0.42      0.50       132
      talk.religion.misc       0.71      0.76      0.74       396
                 sci.med       0.35      0.05      0.09       177
               sci.crypt       0.53      0.41      0.46       396
 comp.os.ms-windows.misc       0.72      0.60      0.66       394
           comp.graphics       0.61      0.80      0.69       398
         sci.electronics       0.48      0.76      0.59       318
      rec.sport.baseball       0.87      0.48      0.61       311
          comp.windows.x       0.57      0.56      0.57       310
  soc.religion.christian       0.46      0.47      0.47       251

             avg / total       0.56      0.53      0.52      6307

F1_SCORE (Macro): 0.506443745288
---- Fin ejecucion ----
