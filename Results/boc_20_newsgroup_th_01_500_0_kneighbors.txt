---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoC
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: jaccard
TFIDF: no
NEIGHBORS: 250
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
Obteniendo documents de training de una parte del corpus boc_20_newsgroup
Obteniendo documents de test de todo el corpus boc_20_newsgroup
--- METODO: get_unique_words() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.17      0.63      0.27       232
      talk.politics.guns       0.22      0.37      0.27       389
      talk.politics.misc       0.35      0.33      0.34       394
             alt.atheism       0.18      0.39      0.24       401
               rec.autos       0.41      0.50      0.45       385
        rec.sport.hockey       0.39      0.58      0.47       395
         rec.motorcycles       0.81      0.07      0.12       325
               sci.space       0.37      0.46      0.41       396
   comp.sys.mac.hardware       0.97      0.25      0.39       154
            misc.forsale       0.55      0.25      0.35       153
   talk.politics.mideast       1.00      0.03      0.06       132
      talk.religion.misc       0.73      0.52      0.61       396
                 sci.med       0.00      0.00      0.00       177
               sci.crypt       0.37      0.17      0.23       396
 comp.os.ms-windows.misc       0.66      0.47      0.55       394
           comp.graphics       0.61      0.59      0.60       398
         sci.electronics       0.47      0.53      0.50       318
      rec.sport.baseball       1.00      0.13      0.23       311
          comp.windows.x       0.46      0.39      0.42       310
  soc.religion.christian       0.49      0.15      0.23       251

             avg / total       0.49      0.37      0.36      6307

F1_SCORE (Macro): 0.337938301629
---- Fin ejecucion ----
