---- EXPERIMENT DETAILS ----
CORPUS: oercommons
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: multilabel
METRIC: cosine
TFIDF: no
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 11000
#TEST: 200

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_multilabel_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_oercommons
Obteniendo documents de test de una parte del corpus bow_oercommons
--- METODO: get_unique_words_bow() ---
----- ALGORITMO Multilabel ------
Creando Vectores de training...
Creando Vectores features de training...
Entrenando clasificador Multilabel
Clasificando en categorias los documentos de test (Prediccion)
Metricas:
----------------
Hamming Loss:
0.0971428571429

Accuracy:
0.23

Precision:
0.53747755339

Recall:
0.545871559633

Classification Report:
                            precision    recall  f1-score   support

                      Arts       0.80      0.57      0.67         7
                 Education       0.61      0.65      0.63        66
                  Business       0.67      1.00      0.80         2
                Humanities       0.54      0.54      0.54        24
Mathematics and Statistics       0.71      0.50      0.59        20
                   Physics       0.48      0.57      0.52        21
                Geoscience       0.52      0.52      0.52        25
 Computing and Information       0.67      0.40      0.50         5
                   Ecology       0.33      0.33      0.33        12
               Engineering       0.12      0.11      0.12         9
    Science and Technology       0.59      0.46      0.52        56
  Forestry and Agriculture       0.33      0.33      0.33        12
             Space Science       0.50      0.69      0.58        13
               Mathematics       0.53      0.57      0.55        14
              Life Science       0.59      0.75      0.66        51
                  Politics       0.50      0.43      0.46         7
                       Law       0.50      0.43      0.46         7
                Technology       0.11      0.17      0.13         6
           Social Sciences       0.50      0.56      0.53        45
                 Chemistry       0.48      0.52      0.50        27
                   History       0.50      0.43      0.46         7

               avg / total       0.54      0.55      0.54       436

F1-score
0.49583227602
----------------
F1_SCORE (Macro): 0.49583227602
---- Fin ejecucion ----
