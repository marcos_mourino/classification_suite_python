---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: jaccard
TFIDF: no
NEIGHBORS: 20
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_20_newsgroup
Obteniendo documents de test de todo el corpus bow_20_newsgroup
--- METODO: get_unique_words_bow() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.36      0.62      0.46       232
      talk.politics.guns       0.29      0.41      0.34       389
      talk.politics.misc       0.35      0.40      0.38       394
             alt.atheism       0.35      0.39      0.37       401
               rec.autos       0.29      0.45      0.35       385
        rec.sport.hockey       0.48      0.35      0.40       395
         rec.motorcycles       0.76      0.42      0.55       325
               sci.space       0.42      0.51      0.46       396
   comp.sys.mac.hardware       0.70      0.43      0.53       154
            misc.forsale       0.53      0.25      0.35       153
   talk.politics.mideast       0.58      0.21      0.31       132
      talk.religion.misc       0.66      0.72      0.69       396
                 sci.med       0.20      0.06      0.09       177
               sci.crypt       0.44      0.36      0.39       396
 comp.os.ms-windows.misc       0.71      0.61      0.65       394
           comp.graphics       0.74      0.78      0.76       398
         sci.electronics       0.48      0.69      0.57       318
      rec.sport.baseball       0.86      0.37      0.52       311
          comp.windows.x       0.60      0.53      0.57       310
  soc.religion.christian       0.45      0.50      0.47       251

             avg / total       0.51      0.48      0.48      6307

F1_SCORE (Macro): 0.459528608678
---- Fin ejecucion ----
