---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: jaccard
TFIDF: no
NEIGHBORS: 5
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_20_newsgroup
Obteniendo documents de test de todo el corpus bow_20_newsgroup
--- METODO: get_unique_words_bow() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.36      0.66      0.46       232
      talk.politics.guns       0.33      0.31      0.32       389
      talk.politics.misc       0.40      0.37      0.38       394
             alt.atheism       0.26      0.48      0.34       401
               rec.autos       0.34      0.42      0.37       385
        rec.sport.hockey       0.50      0.33      0.40       395
         rec.motorcycles       0.62      0.29      0.39       325
               sci.space       0.40      0.48      0.43       396
   comp.sys.mac.hardware       0.58      0.48      0.52       154
            misc.forsale       0.55      0.22      0.31       153
   talk.politics.mideast       0.39      0.30      0.34       132
      talk.religion.misc       0.73      0.65      0.69       396
                 sci.med       0.23      0.09      0.13       177
               sci.crypt       0.49      0.37      0.42       396
 comp.os.ms-windows.misc       0.68      0.59      0.63       394
           comp.graphics       0.77      0.68      0.72       398
         sci.electronics       0.41      0.72      0.52       318
      rec.sport.baseball       0.84      0.43      0.57       311
          comp.windows.x       0.48      0.58      0.53       310
  soc.religion.christian       0.52      0.52      0.52       251

             avg / total       0.50      0.46      0.46      6307

F1_SCORE (Macro): 0.450345292361
---- Fin ejecucion ----
