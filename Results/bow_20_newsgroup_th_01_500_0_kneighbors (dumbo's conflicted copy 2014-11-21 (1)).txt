---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: braycurtis
TFIDF: no
NEIGHBORS: 5
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_20_newsgroup
Obteniendo documents de test de todo el corpus bow_20_newsgroup
--- METODO: get_unique_words_bow() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.33      0.63      0.43       232
      talk.politics.guns       0.46      0.35      0.40       389
      talk.politics.misc       0.47      0.43      0.45       394
             alt.atheism       0.34      0.54      0.42       401
               rec.autos       0.38      0.52      0.44       385
        rec.sport.hockey       0.61      0.42      0.50       395
         rec.motorcycles       0.75      0.32      0.45       325
               sci.space       0.52      0.59      0.56       396
   comp.sys.mac.hardware       0.72      0.56      0.63       154
            misc.forsale       0.71      0.31      0.43       153
   talk.politics.mideast       0.57      0.46      0.51       132
      talk.religion.misc       0.72      0.75      0.74       396
                 sci.med       0.24      0.06      0.09       177
               sci.crypt       0.57      0.40      0.47       396
 comp.os.ms-windows.misc       0.72      0.61      0.66       394
           comp.graphics       0.64      0.77      0.70       398
         sci.electronics       0.44      0.75      0.56       318
      rec.sport.baseball       0.88      0.51      0.65       311
          comp.windows.x       0.49      0.56      0.52       310
  soc.religion.christian       0.50      0.51      0.50       251

             avg / total       0.55      0.52      0.52      6307

F1_SCORE (Macro): 0.505297212816
---- Fin ejecucion ----
