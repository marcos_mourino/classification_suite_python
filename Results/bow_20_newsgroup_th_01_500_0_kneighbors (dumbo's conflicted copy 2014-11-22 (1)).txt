---- EXPERIMENT DETAILS ----
CORPUS: 20_newsgroup
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: kneighbors
METRIC: braycurtis
TFIDF: no
NEIGHBORS: 100
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_20_newsgroup
Obteniendo documents de test de todo el corpus bow_20_newsgroup
--- METODO: get_unique_words_bow() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                          precision    recall  f1-score   support

comp.sys.ibm.pc.hardware       0.25      0.49      0.33       232
      talk.politics.guns       0.32      0.47      0.38       389
      talk.politics.misc       0.52      0.52      0.52       394
             alt.atheism       0.55      0.33      0.41       401
               rec.autos       0.33      0.65      0.44       385
        rec.sport.hockey       0.65      0.44      0.53       395
         rec.motorcycles       0.86      0.38      0.53       325
               sci.space       0.56      0.61      0.59       396
   comp.sys.mac.hardware       0.94      0.21      0.34       154
            misc.forsale       0.54      0.24      0.33       153
   talk.politics.mideast       0.96      0.18      0.31       132
      talk.religion.misc       0.58      0.76      0.65       396
                 sci.med       1.00      0.01      0.01       177
               sci.crypt       0.46      0.41      0.43       396
 comp.os.ms-windows.misc       0.80      0.47      0.60       394
           comp.graphics       0.37      0.88      0.52       398
         sci.electronics       0.50      0.65      0.56       318
      rec.sport.baseball       0.94      0.26      0.41       311
          comp.windows.x       0.58      0.41      0.48       310
  soc.religion.christian       0.44      0.22      0.29       251

             avg / total       0.58      0.47      0.46      6307

F1_SCORE (Macro): 0.432793267073
---- Fin ejecucion ----
