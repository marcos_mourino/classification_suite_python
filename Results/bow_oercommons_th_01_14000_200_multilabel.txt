---- EXPERIMENT DETAILS ----
CORPUS: oercommons
REPRESENTATION: BoW
THRESHOLD: 0.01
CLASSIFY METHOD: multilabel
METRIC: cosine
TFIDF: no
STEMMING: porter
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 14000
#TEST: 200

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_multilabel_database_bow() ---
Obteniendo documents de training de una parte del corpus bow_oercommons
Obteniendo documents de test de una parte del corpus bow_oercommons
--- METODO: get_unique_words_bow() ---
----- ALGORITMO Multilabel ------
Creando Vectores de training...
Creando Vectores features de training...
Entrenando clasificador Multilabel
Clasificando en categorias los documentos de test (Prediccion)
Metricas:
----------------
Hamming Loss:
0.092619047619

Accuracy:
0.24

Precision:
0.56657786215

Recall:
0.55504587156

Classification Report:
                            precision    recall  f1-score   support

                      Arts       0.83      0.71      0.77         7
                 Education       0.61      0.64      0.62        66
                  Business       0.50      1.00      0.67         2
                Humanities       0.58      0.58      0.58        24
Mathematics and Statistics       0.92      0.60      0.73        20
                   Physics       0.52      0.52      0.52        21
                Geoscience       0.52      0.52      0.52        25
 Computing and Information       0.67      0.40      0.50         5
                   Ecology       0.36      0.42      0.38        12
               Engineering       0.43      0.33      0.38         9
    Science and Technology       0.64      0.48      0.55        56
  Forestry and Agriculture       0.36      0.42      0.38        12
             Space Science       0.57      0.62      0.59        13
               Mathematics       0.56      0.36      0.43        14
              Life Science       0.56      0.73      0.63        51
                  Politics       0.57      0.57      0.57         7
                       Law       0.57      0.57      0.57         7
                Technology       0.00      0.00      0.00         6
           Social Sciences       0.50      0.58      0.54        45
                 Chemistry       0.50      0.48      0.49        27
                   History       0.57      0.57      0.57         7

               avg / total       0.57      0.56      0.55       436

F1-score
0.524217832378
----------------
F1_SCORE (Macro): 0.524217832378
---- Fin ejecucion ----
