---- EXPERIMENT DETAILS ----
CORPUS: reuters_21578_expanded
REPRESENTATION: BoC
THRESHOLD: 0.01
EXPANSION THRESHOLD: 0.8
EXPANSION RELATEDNESS: 0.8
EXPANDED WEIGHTING: 0.1
CLASSIFY METHOD: kneighbors
METRIC: braycurtis
TFIDF: no
NEIGHBORS: 10
STEMMING: -
SMOOTHING: 0.001
WEIGHTING: milne
#TRAIN: 500
#TEST: all

Obteniendo documents de training y de testing...
--- METODO: get_documents_from_database_boc() ---
0.1
Obteniendo documents de training de una parte del corpus boc_reuters_21578_expanded
Obteniendo documents de test de todo el corpus boc_reuters_21578_expanded
--- METODO: get_unique_words() ---

----- ALGORITMO KNeighbours ------
Creando Vectores de training...
Entrenando el algoritmo...
Calculando metricas ...
                 precision    recall  f1-score   support

          cocoa       0.87      0.81      0.84       540
           earn       0.60      0.40      0.48        15
            acq       0.67      0.33      0.44         6
         copper       0.00      0.00      0.00         5
        housing       0.53      0.75      0.62        12
   money-supply       0.00      0.00      0.00         1
         coffee       0.71      0.80      0.75        15
          sugar       0.73      0.73      0.73        11
          trade       1.00      0.50      0.67         4
       reserves       0.50      0.72      0.59        18
           ship       0.00      0.00      0.00         1
         cotton       0.74      0.85      0.79       129
          grain       0.00      0.00      0.00         1
          crude       0.92      0.96      0.94       624
        nat-gas       0.00      0.00      0.00         5
            cpi       0.00      0.00      0.00         9
       interest       0.70      0.35      0.47        20
       money-fx       0.58      0.78      0.67        18
           alum       0.50      0.33      0.40         3
            tin       0.00      0.00      0.00         1
           gold       0.75      1.00      0.86         3
strategic-metal       0.00      0.00      0.00         1
         retail       1.00      0.67      0.80         3
            ipi       0.00      0.00      0.00         5
        oilseed       0.00      0.00      0.00         1
     iron-steel       0.43      0.46      0.45        63
         rubber       0.42      0.79      0.55        14
           heat       0.86      0.40      0.55        15
           jobs       0.00      0.00      0.00         1
            lei       0.86      0.60      0.71        10
            bop       0.00      0.00      0.00         0
            gnp       0.12      0.50      0.20         2
           zinc       0.00      0.00      0.00         5
        veg-oil       0.52      0.49      0.51        73
         orange       0.35      0.70      0.47        20
        carcass       0.00      0.00      0.00         1
       pet-chem       0.67      0.12      0.21        16
            gas       0.00      0.00      0.00         7
            wpi       0.00      0.00      0.00         2
      livestock       0.00      0.00      0.00         0
         lumber       0.00      0.00      0.00         1
    instal-debt       0.79      0.85      0.81        13
      meal-feed       1.00      1.00      1.00         1
           lead       1.00      0.43      0.60         7
         potato       0.65      0.33      0.44        45
         nickel       0.00      0.00      0.00         6
    inventories       0.00      0.00      0.00         5
            cpu       0.47      0.77      0.58        26
       l-cattle       0.00      0.00      0.00         3
         silver       1.00      0.17      0.29         6
           fuel       0.47      0.93      0.62        75
            jet       0.00      0.00      0.00        18
         income       0.67      0.29      0.40         7
           rand       1.00      0.17      0.29         6

    avg / total       0.76      0.77      0.75      1899

F1_SCORE (Macro): 0.346372165329
---- Fin ejecucion ----
