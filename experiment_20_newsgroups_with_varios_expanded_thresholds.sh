python classify.py -corpus boc_20_newsgroup -method mbayes -train 5
python classify.py -corpus boc_20_newsgroup -method mbayes -train 10
python classify.py -corpus boc_20_newsgroup -method mbayes -train 15
python classify.py -corpus boc_20_newsgroup -method mbayes -train 20
python classify.py -corpus boc_20_newsgroup -method mbayes -train 25
python classify.py -corpus boc_20_newsgroup -method mbayes -train 30
python classify.py -corpus boc_20_newsgroup -method mbayes -train 40
python classify.py -corpus boc_20_newsgroup -method mbayes -train 50
python classify.py -corpus boc_20_newsgroup -method mbayes -train 75
python classify.py -corpus boc_20_newsgroup -method mbayes -train 100
python classify.py -corpus boc_20_newsgroup -method mbayes -train 150

python classify.py -corpus bow_20_newsgroup -method mbayes -train 5
python classify.py -corpus bow_20_newsgroup -method mbayes -train 10
python classify.py -corpus bow_20_newsgroup -method mbayes -train 15
python classify.py -corpus bow_20_newsgroup -method mbayes -train 20
python classify.py -corpus bow_20_newsgroup -method mbayes -train 25
python classify.py -corpus bow_20_newsgroup -method mbayes -train 30
python classify.py -corpus bow_20_newsgroup -method mbayes -train 40
python classify.py -corpus bow_20_newsgroup -method mbayes -train 50
python classify.py -corpus bow_20_newsgroup -method mbayes -train 75
python classify.py -corpus bow_20_newsgroup -method mbayes -train 100
python classify.py -corpus bow_20_newsgroup -method mbayes -train 150

python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 5 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 10 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 15 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 20 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 25 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 30 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 40 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 50 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 75 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 100 -exp_threshold 0.9 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 150 -exp_threshold 0.9 -exp_relatedness 0.9

python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.8

python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 5 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 10 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 15 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 20 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 25 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 30 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 40 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 50 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 75 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 100 -exp_threshold 0.8 -exp_relatedness 0.9
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 150 -exp_threshold 0.8 -exp_relatedness 0.9

python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 5 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 10 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 15 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 20 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 25 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 30 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 40 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 50 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 75 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 100 -exp_threshold 0.9 -exp_relatedness 0.8
python classify.py -corpus boc_20_newsgroup_expanded -method mbayes -train 150 -exp_threshold 0.9 -exp_relatedness 0.8

python display_graphic_20_newsgroups_various_expansions.py experiment_20_newsgroups_with_varios_expanded_thresholds



